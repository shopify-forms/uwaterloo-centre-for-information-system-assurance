<?php
/**
 * @file
 *
 * Mapping file that is used form webform options as token value.
 */

/**
 * Shopify mapping file
 *
 * Create an array of variants ids as keys and quantity as value and pass it to
 * shopify using multipass. Expected result would be checkout page on shopify.
 *
 * Variable $products holds variants ids from Shopify and also option keys from
 * webform Payment field. Any update to webform (eg. adding additional payment
 * option) payment field requires update be made here.
 * eg. https://<shopify_url>/admin/products/9912004353.json
 *
 *
 * @param array $values
 *   Webform submission values are used to create mapping array for checkout
 *
 * @return array
 *   Array that would hold variants ids as keys and quantity as array values
 */
function uw_forms_get_varients_uwcisa(array $values) {
  $order = array();

  // See variable $product description in function comment
  $products = array(
    'workshop' => '36843952193',
    'symposium' => '36843952257',
    'both' => '36843952321',
  );

  $event = isset($values['payment'][0])? $values['payment'][0] : FALSE;

  // Currently all quantities are set to 1. There is no quantity webform field.
  if ($event && isset($products[$event])) {
    $order[$products[$event]] = 1;
  }

  return $order;
}
